﻿namespace ZeusMapCollisionEditor
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.listView_file = new System.Windows.Forms.ListView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button_clear = new System.Windows.Forms.Button();
            this.button_delSel = new System.Windows.Forms.Button();
            this.button_add = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButton_bucket = new System.Windows.Forms.RadioButton();
            this.checkBox_prop = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_h = new System.Windows.Forms.TextBox();
            this.textBox_w = new System.Windows.Forms.TextBox();
            this.radioButton_eraser = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.button_default = new System.Windows.Forms.Button();
            this.textBox_percent = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.radioButton_brush = new System.Windows.Forms.RadioButton();
            this.radioButton_move = new System.Windows.Forms.RadioButton();
            this.myButton1 = new ZeusMapCollisionEditor.MyButton();
            this.label3 = new System.Windows.Forms.Label();
            this.button_refresh = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView_file
            // 
            this.listView_file.GridLines = true;
            this.listView_file.Location = new System.Drawing.Point(6, 20);
            this.listView_file.Name = "listView_file";
            this.listView_file.Size = new System.Drawing.Size(373, 111);
            this.listView_file.TabIndex = 0;
            this.listView_file.UseCompatibleStateImageBehavior = false;
            this.listView_file.View = System.Windows.Forms.View.List;
            this.listView_file.SelectedIndexChanged += new System.EventHandler(this.listView_file_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button_clear);
            this.groupBox1.Controls.Add(this.button_delSel);
            this.groupBox1.Controls.Add(this.button_add);
            this.groupBox1.Controls.Add(this.listView_file);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(522, 146);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "选择编辑的地图";
            // 
            // button_clear
            // 
            this.button_clear.Location = new System.Drawing.Point(396, 102);
            this.button_clear.Name = "button_clear";
            this.button_clear.Size = new System.Drawing.Size(120, 28);
            this.button_clear.TabIndex = 3;
            this.button_clear.Text = "清空";
            this.button_clear.UseVisualStyleBackColor = true;
            this.button_clear.Click += new System.EventHandler(this.button_clear_Click);
            // 
            // button_delSel
            // 
            this.button_delSel.Location = new System.Drawing.Point(396, 61);
            this.button_delSel.Name = "button_delSel";
            this.button_delSel.Size = new System.Drawing.Size(120, 28);
            this.button_delSel.TabIndex = 2;
            this.button_delSel.Text = "删除选中";
            this.button_delSel.UseVisualStyleBackColor = true;
            this.button_delSel.Click += new System.EventHandler(this.button_delSel_Click);
            // 
            // button_add
            // 
            this.button_add.Location = new System.Drawing.Point(396, 20);
            this.button_add.Name = "button_add";
            this.button_add.Size = new System.Drawing.Size(120, 28);
            this.button_add.TabIndex = 1;
            this.button_add.Text = "添加";
            this.button_add.UseVisualStyleBackColor = true;
            this.button_add.Click += new System.EventHandler(this.button_add_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButton_bucket);
            this.groupBox2.Controls.Add(this.checkBox_prop);
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.radioButton_eraser);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.radioButton_brush);
            this.groupBox2.Controls.Add(this.radioButton_move);
            this.groupBox2.Controls.Add(this.myButton1);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.button_refresh);
            this.groupBox2.Location = new System.Drawing.Point(540, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(410, 146);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "工具和参数";
            // 
            // radioButton_bucket
            // 
            this.radioButton_bucket.AutoSize = true;
            this.radioButton_bucket.Location = new System.Drawing.Point(18, 120);
            this.radioButton_bucket.Name = "radioButton_bucket";
            this.radioButton_bucket.Size = new System.Drawing.Size(71, 16);
            this.radioButton_bucket.TabIndex = 14;
            this.radioButton_bucket.TabStop = true;
            this.radioButton_bucket.Text = "填充工具";
            this.radioButton_bucket.UseVisualStyleBackColor = true;
            // 
            // checkBox_prop
            // 
            this.checkBox_prop.AutoSize = true;
            this.checkBox_prop.Checked = true;
            this.checkBox_prop.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_prop.Location = new System.Drawing.Point(110, 127);
            this.checkBox_prop.Name = "checkBox_prop";
            this.checkBox_prop.Size = new System.Drawing.Size(102, 16);
            this.checkBox_prop.TabIndex = 13;
            this.checkBox_prop.Text = "约束比例(1:1)";
            this.checkBox_prop.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.textBox_h);
            this.groupBox4.Controls.Add(this.textBox_w);
            this.groupBox4.Location = new System.Drawing.Point(95, 52);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(117, 73);
            this.groupBox4.TabIndex = 12;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "网格尺寸";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "高:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(122, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "×";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(23, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "宽:";
            // 
            // textBox_h
            // 
            this.textBox_h.Location = new System.Drawing.Point(54, 48);
            this.textBox_h.Name = "textBox_h";
            this.textBox_h.Size = new System.Drawing.Size(35, 21);
            this.textBox_h.TabIndex = 2;
            this.textBox_h.Text = "20";
            this.textBox_h.TextChanged += new System.EventHandler(this.textBox_h_TextChanged);
            this.textBox_h.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_h_KeyPress);
            // 
            // textBox_w
            // 
            this.textBox_w.Location = new System.Drawing.Point(54, 20);
            this.textBox_w.Name = "textBox_w";
            this.textBox_w.Size = new System.Drawing.Size(35, 21);
            this.textBox_w.TabIndex = 1;
            this.textBox_w.Text = "20";
            this.textBox_w.TextChanged += new System.EventHandler(this.textBox_w_TextChanged);
            this.textBox_w.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_w_KeyPress);
            // 
            // radioButton_eraser
            // 
            this.radioButton_eraser.AutoSize = true;
            this.radioButton_eraser.Location = new System.Drawing.Point(18, 98);
            this.radioButton_eraser.Name = "radioButton_eraser";
            this.radioButton_eraser.Size = new System.Drawing.Size(71, 16);
            this.radioButton_eraser.TabIndex = 11;
            this.radioButton_eraser.Text = "橡皮工具";
            this.radioButton_eraser.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.button_default);
            this.groupBox3.Controls.Add(this.textBox_percent);
            this.groupBox3.Location = new System.Drawing.Point(219, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(185, 90);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "比例(Ctrl + 滚轴)";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(96, 51);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(83, 31);
            this.button3.TabIndex = 4;
            this.button3.Text = "缩小";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(11, 50);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(78, 32);
            this.button2.TabIndex = 3;
            this.button2.Text = "放大";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(57, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "%";
            // 
            // button_default
            // 
            this.button_default.Location = new System.Drawing.Point(96, 23);
            this.button_default.Name = "button_default";
            this.button_default.Size = new System.Drawing.Size(83, 23);
            this.button_default.TabIndex = 1;
            this.button_default.Text = "默认";
            this.button_default.UseVisualStyleBackColor = true;
            this.button_default.Click += new System.EventHandler(this.button_default_Click);
            // 
            // textBox_percent
            // 
            this.textBox_percent.Location = new System.Drawing.Point(11, 23);
            this.textBox_percent.Name = "textBox_percent";
            this.textBox_percent.Size = new System.Drawing.Size(40, 21);
            this.textBox_percent.TabIndex = 0;
            this.textBox_percent.Text = "100";
            this.textBox_percent.TextChanged += new System.EventHandler(this.textBox_percent_TextChanged);
            this.textBox_percent.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_percent_KeyPress);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(315, 108);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(90, 32);
            this.button1.TabIndex = 9;
            this.button1.Text = "生成";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // radioButton_brush
            // 
            this.radioButton_brush.AutoSize = true;
            this.radioButton_brush.Checked = true;
            this.radioButton_brush.Location = new System.Drawing.Point(18, 76);
            this.radioButton_brush.Name = "radioButton_brush";
            this.radioButton_brush.Size = new System.Drawing.Size(71, 16);
            this.radioButton_brush.TabIndex = 8;
            this.radioButton_brush.TabStop = true;
            this.radioButton_brush.Text = "画笔工具";
            this.radioButton_brush.UseVisualStyleBackColor = true;
            // 
            // radioButton_move
            // 
            this.radioButton_move.AutoSize = true;
            this.radioButton_move.Location = new System.Drawing.Point(18, 54);
            this.radioButton_move.Name = "radioButton_move";
            this.radioButton_move.Size = new System.Drawing.Size(71, 16);
            this.radioButton_move.TabIndex = 7;
            this.radioButton_move.Text = "移动工具";
            this.radioButton_move.UseVisualStyleBackColor = true;
            // 
            // myButton1
            // 
            this.myButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.myButton1.Location = new System.Drawing.Point(117, 23);
            this.myButton1.Name = "myButton1";
            this.myButton1.Size = new System.Drawing.Size(75, 23);
            this.myButton1.TabIndex = 6;
            this.myButton1.UseVisualStyleBackColor = true;
            this.myButton1.Click += new System.EventHandler(this.myButton1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "碰撞区颜色表示:";
            // 
            // button_refresh
            // 
            this.button_refresh.Location = new System.Drawing.Point(218, 109);
            this.button_refresh.Name = "button_refresh";
            this.button_refresh.Size = new System.Drawing.Size(90, 32);
            this.button_refresh.TabIndex = 4;
            this.button_refresh.Text = "刷新";
            this.button_refresh.UseVisualStyleBackColor = true;
            this.button_refresh.Click += new System.EventHandler(this.button_refresh_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Multiselect = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(964, 749);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "ZeusMapCollisionEditor";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseLeave += new System.EventHandler(this.Form1_MouseLeave);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView_file;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button_clear;
        private System.Windows.Forms.Button button_delSel;
        private System.Windows.Forms.Button button_add;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_refresh;
        private System.Windows.Forms.TextBox textBox_h;
        private System.Windows.Forms.TextBox textBox_w;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private MyButton myButton1;
        private System.Windows.Forms.RadioButton radioButton_brush;
        private System.Windows.Forms.RadioButton radioButton_move;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button_default;
        private System.Windows.Forms.TextBox textBox_percent;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton radioButton_eraser;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.CheckBox checkBox_prop;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton radioButton_bucket;
        private System.Windows.Forms.Label label5;
    }
}

