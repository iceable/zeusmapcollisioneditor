#ZeusMapCollisionEditor
ZeusMapCollisionEditor是一个用于叫做Zeus的MMORPG的关于地图碰撞编辑的编辑器。
使用方法如下：

1.  添加一个PNG图片作为地图。
2.  按照需要大小将地图分割为N个小格子，称作Cell。
3.  将需要的部分填充颜色，表面这个Cell是碰撞区。
4.  将完成的碰撞图片保存为碰撞地图文件（*.collision）。